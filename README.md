# Description
Repository for CI/CD Bibit Usecase.

# Output
1. This repository is a sample for build and deploy application for Docker container engine
2. `build` stage describe about how to create an application image (ex: nginx web server)
3. `deploy` stage describe about how to deploy the application image that built on `build` stage via SSH protocol to run the docker command to run the container image
4. There is required environment variables needed for this sample CI/CD, such the followings:
    * `SSH_USER`: is a target machine user
    * `SSH_TARGET`: is a target machine to deploy the application
    * `SSH_PRIVATE_KEY`: is a base64-encoded string for the SSH private key to be used to connect
5. The required environment variables is intended to be stored under [CI/CD runtime environment variables](https://gitlab.com/ardikabs/bibit-cicd/-/settings/ci_cd) (nb: you need permission to open the page).
6. For the record, i using [nginx](https://hub.docker.com/_/nginx), so it is not using `/var/www/html` instead `/usr/share/nginx/html`.